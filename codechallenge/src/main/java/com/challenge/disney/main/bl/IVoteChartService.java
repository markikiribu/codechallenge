package com.challenge.disney.main.bl;

import com.challenge.disney.main.request.ResponseCodeRequest;

public interface IVoteChartService {

	ResponseCodeRequest voteDogPicture(String userId, String dogPicId, String vote);

}