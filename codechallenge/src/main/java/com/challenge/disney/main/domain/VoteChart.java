package com.challenge.disney.main.domain;

import com.challenge.disney.main.domain.ids.VoteChartCompositeId;
public class VoteChart {

	private VoteChartCompositeId id;

	private String vote;

	public VoteChart() {
		super();
	}
	
	public VoteChart(AppUser appUser, DogPicture dogPicture, String vote) {
		super();
		this.vote = vote;
		this.id = new VoteChartCompositeId(appUser, dogPicture);
	}

	public VoteChartCompositeId getId() {
		return id;
	}

	public void setId(VoteChartCompositeId id) {
		this.id = id;
	}

	public String getVote() {
		return vote;
	}

	public void setVote(String vote) {
		this.vote = vote;
	}

}