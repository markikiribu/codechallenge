package com.challenge.disney.main.bl.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.disney.main.bl.IVoteChartService;
import com.challenge.disney.main.domain.DogPicture;
import com.challenge.disney.main.repository.IAppUserDAO;
import com.challenge.disney.main.repository.IDogPictureDAO;
import com.challenge.disney.main.request.ResponseCodeRequest;

@Service
public class VoteChartServiceImpl implements IVoteChartService {

	@Autowired
	private IDogPictureDAO dogPictureDao;

	@Autowired
	private IAppUserDAO appUserDao;

	@Override
	public ResponseCodeRequest voteDogPicture(String userId, String dogPicId, String vote) {
		DogPicture picture = this.dogPictureDao.getDogPictureByIdentifierCode(dogPicId);
		if ("LIKE".equals(vote.trim().toUpperCase())) {
			picture.setUpVotes((picture.getUpVotes() == null ? 0 : picture.getUpVotes()) + 1);
		} else {
			picture.setDownVotes((picture.getDownVotes() == null ? 0 : picture.getDownVotes()) + 1);
		}
		this.dogPictureDao.save(picture);
		return new ResponseCodeRequest("SUCCES", picture.getIdentifierCode());
	}

}
