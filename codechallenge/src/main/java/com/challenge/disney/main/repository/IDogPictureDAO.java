package com.challenge.disney.main.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.challenge.disney.main.domain.DogPicture;

public interface IDogPictureDAO extends JpaRepository<DogPicture, Long> {

	List<DogPicture> getAllDogsByBreed(String breed);
	
	DogPicture getDogPictureByIdentifierCode(String identifierCode);

}