package com.challenge.disney.main.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.disney.main.bl.IDogPictureService;
import com.challenge.disney.main.bl.IVoteChartService;
import com.challenge.disney.main.domain.DogPicture;
import com.challenge.disney.main.request.ResponseCodeRequest;

@RestController
@RequestMapping("/dog_gallery")
public class DogGalleryController {
	
	@Autowired
	IDogPictureService dogPictureService;
	
	@Autowired
	IVoteChartService voteChartService;

	@RequestMapping(method = RequestMethod.GET, path = "/all", produces = "application/json")
	public Map<String, List<DogPicture>> getAllDogPictures() {
		return this.dogPictureService.getAllPicturesByBreed();
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/breed", produces = "application/json")
	public List<DogPicture> getAllDogPicturesbyBreed(@RequestParam(name="breed") String dogBreed) {
		return this.dogPictureService.getAllPicturesByBreed(dogBreed);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/vote", produces = "application/json")
	public ResponseCodeRequest upVoteDogPicture(@RequestParam("userId")String userId, @RequestParam("dogPicId")String dogPicId, @RequestParam("vote")String vote) {
		return this.voteChartService.voteDogPicture(userId, dogPicId, vote);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/add_picture", produces = "application/json")
	public ResponseCodeRequest addDogPicture(@RequestParam("pictureUrl")String pictureUrl, @RequestParam("breed")String breed, @RequestParam("description")String description ) {
		return this.dogPictureService.addDogPicture(new DogPicture(pictureUrl, breed, description));
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/all_breeds", produces = "application/json")
	public ResponseEntity<?> getAllDOgPictures(){
		return new ResponseEntity<>(this.dogPictureService.getAllPicturesByBreed(), HttpStatus.BAD_REQUEST);
	}

}
