package com.challenge.disney.main.request;

public class ResponseCodeRequest {

	private String code;
	private String description;

	public ResponseCodeRequest() {
		super();
	}

	public ResponseCodeRequest(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}