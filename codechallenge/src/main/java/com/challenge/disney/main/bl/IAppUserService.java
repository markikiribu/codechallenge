package com.challenge.disney.main.bl;

import java.util.List;

import com.challenge.disney.main.domain.AppUser;
import com.challenge.disney.main.request.ResponseCodeRequest;

public interface IAppUserService {
	
	List<AppUser> listAllUsers();

	ResponseCodeRequest createNewUser(AppUser user);

}
