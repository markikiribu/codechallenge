package com.challenge.disney.main.bl.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.challenge.disney.main.bl.IDogPictureService;
import com.challenge.disney.main.domain.DogPicture;
import com.challenge.disney.main.repository.IDogPictureDAO;
import com.challenge.disney.main.request.ResponseCodeRequest;

@Service
public class DogPictureServiceImpl implements IDogPictureService {

	public static final int ZERO_UPDATE = 0;
	public static final int ONE_ROW_UPDATE = 1;

	@Autowired
	private IDogPictureDAO dogPictureDao;

	@Override
	public Map<String, List<DogPicture>> getAllPicturesByBreed() {
		List<DogPicture> gallery = this.dogPictureDao.findAll();

		Map<String, List<DogPicture>> result = new HashMap<String, List<DogPicture>>();

		for (DogPicture dog : gallery) {
			if (result.containsKey(dog.getBreed())) {
				result.get(dog.getBreed()).add(dog);
			} else {
				result.put(dog.getBreed(), new ArrayList<DogPicture>());
				result.get(dog.getBreed()).add(dog);
			}
		}
		
		for(String key : result.keySet()){
			Collections.sort(result.get(key), (dog1, dog2) -> dog2.getBreed().compareTo(dog1.getBreed()));
		}

		return result;
	}

	@Override
	public List<DogPicture> getAllPicturesByBreed(String breed) {
		List<DogPicture> gallery = this.dogPictureDao.getAllDogsByBreed(breed);
		Collections.sort(gallery, (dog1, dog2) -> dog2.getBreed().compareTo(dog1.getBreed()));
		return gallery;
	}

	@Override
	public ResponseCodeRequest addDogPicture(DogPicture dogPicture) {
		this.dogPictureDao.save(dogPicture);
		return new ResponseCodeRequest("SUCCESS", dogPicture.getIdentifierCode());
	}

}
