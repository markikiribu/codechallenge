package com.challenge.disney.main.domain.ids;

import com.challenge.disney.main.domain.AppUser;
import com.challenge.disney.main.domain.DogPicture;

public class VoteChartCompositeId {

	private AppUser appUser;

	private DogPicture dogPicture;

	public AppUser getAppUser() {
		return appUser;
	}

	public VoteChartCompositeId(AppUser appUser, DogPicture dogPicture) {
		super();
		this.appUser = appUser;
		this.dogPicture = dogPicture;
	}

	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}

	public DogPicture getDogPicture() {
		return dogPicture;
	}

	public void setDogPicture(DogPicture dogPicture) {
		this.dogPicture = dogPicture;
	}

	public VoteChartCompositeId() {
		super();
	}

}
