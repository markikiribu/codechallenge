package com.challenge.disney.main.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.challenge.disney.main.bl.IAppUserService;
import com.challenge.disney.main.domain.AppUser;
import com.challenge.disney.main.request.ResponseCodeRequest;

@RestController
@RequestMapping("/app_users")
public class AppUserController {
	
	@Autowired
	private IAppUserService appUserService;
	
	@RequestMapping(method = RequestMethod.GET, path = "/all", produces = "application/json")
	public List<AppUser> getAllDogPictures() {
		return this.appUserService.listAllUsers();
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/add_user")
	public ResponseCodeRequest getAllDogPictures(@RequestParam("name")String name, @RequestParam("surname")String surname) {
		return appUserService.createNewUser(new AppUser(name, surname));
	}

}
